<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	 
	 // creates the 4 tables
	public function up()
	{
		Schema::create('post', function($post){
			$post->increments('id');
			$post->string('name');
			$post->string('title');
			$post->string('message');
		});
		
		Schema::create('comment', function($comment){
			$comment->increments('id');
			$comment->string('name');
			$comment->string('comment');
			$comment->string('postid');
		});
		
		Schema::create('account', function($account){
			$comment->increments('userid');
			$comment->string('email');
			$comment->string('password');
			$comment->string('Fullname');
			$comment->string('DOB');
			$comment->string('profileimage');
			$comment->string('friendslist');
			$comment->string('onlineofline');
		});
		
		Schema::create('friends', function($friends){
			$comment->increments('friendsid');
			$comment->string('fulname');
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	 
	 //drops the 4 tables
	public function down()
	{
		Schema::drop('post');
		Schema::drop('comment');
		Schema::drop('account');
		Schema::drop('friends');
	}

}
