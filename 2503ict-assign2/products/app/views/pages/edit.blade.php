@extends('layouts.master')

@section('content')
    <div class='col-sm-4'>
        <h1>Editing Post:</h1><br><h3><b>Title:</b> <u>{{{ $post['0']->title }}}</u><br> <b>Posted by:</b> <u>{{{ $post['0']->name }}}</u></h3>
    </div>
    <div class='col-sm-8'>
        <div class='box'>
            <img class="image" src="images/takemymoney.jpg" alt="Message Image" />
            <b>Name:</b> {{{ $post['0']->name }}} <br>
            <b>Title:</b> {{{ $post['0']->title }}} <br>
            <b>Message:</b> {{{ $post['0']->message }}} <br>
            <div class='editbuttons'>
                <form method="get" action="updateMessage">
                	<b>Name: </b><br>
                	<input type='text' name='name' value='{{{ $post['0']->name }}}'> <br>
                	<b>Title: </b><br>
                	<input type='text' name='title' value='{{{ $post['0']->title }}}'> <br>
                	<b>Message: </b><br>
            	    <textarea rows='4' cols='22' name='message'>{{{ $post['0']->message }}}</textarea><br>
            	    <table>
                        <tbody>
                            <tr><td><?php echo "<button type='submit' name='edit' value='{$post['0']->id }'>Save</button>";?></td>
                            <td><button>Cancel</button></td></tr>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
@stop