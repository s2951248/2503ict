@extends('layouts.master')

@section('content')
    <div class='col-sm-4'>
        <h1>Post a Message:</h1>
        <form method="post" action="postMessage">
        	Name: <br>
        	<input type='text' name='name'> <br>
        	Title: <br>
        	<input type='text' name='title'> <br>
        	Message: <br>
        	<textarea rows='4' cols='22' name='message'></textarea> <br>
            <input type="submit" value="Submit" />
        </form>
    </div>
    <div class='col-sm-8'>
        <h3>Latest Messages:</h3>
        @if (count($results) == 0)
            <p><b>No messages to display.</b></p>
        @else 
            @for ($i=0; $i < count($results); $i++)
                <div class='box'>
                    <img class="image" src="images/takemymoney.jpg" alt="Message Image" />
                    <b>Name:</b> {{{ $results[$i]->name }}} <br>
                    <b>Title:</b> {{{ $results[$i]->title }}} <br>
                    <b>Message:</b> {{{ $results[$i]->message }}} <br>
                    <table>
                        <tbody>
                            <tr><td><form method="get" action="editpost">
                                <?php echo "<button type='submit' name='editpost' value='{$results[$i]->id }'>Edit</button>";?>
                            </form></td>
                            <td><form method="get" action="commentredirect">
                                <?php echo "<button type='submit' name='commentredirect' value='{$results[$i]->id }'>Comment</button>";?>
                            </form></td>
                            <td><button>Delete</button><br></td></tr>
                        </tbody>
                    </table>
                </div>
            @endfor
        @endif
    </div>
@stop