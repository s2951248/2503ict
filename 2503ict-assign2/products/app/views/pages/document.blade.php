@extends('layouts.master')

@section('content')
    <div class='col-sm-5'>
        <h1>Diagrams:</h1><br>
        <img class="image" src="images/diagram.jpg" alt="ER Diagram and Site Map" style="width:100%;height:100%"/>
    </div>
    <div class='col-sm-7'>
        <h1>Documentation:</h1>
        <object data="files/document.pdf" type="application/pdf" width="100%" height="500">
            <p>It appears you don't have a PDF plugin for this browser.
            No biggie... you can <a href="files/document.pdf">click here to
            download the PDF file.</a></p>
        </object>
    </div>
@stop