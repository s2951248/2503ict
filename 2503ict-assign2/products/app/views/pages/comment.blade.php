@extends('layouts.master')

@section('content')
    <div class='col-sm-4'>
        <h1>Comments of: </h1><br>                
            <img class="image" src="images/takemymoney.jpg" alt="Message Image" />
                <b>Name:</b> {{{ $post['0']->name }}} <br>
                <b>Title:</b> {{{ $post['0']->title }}} <br>
                <b>Message:</b> {{{ $post['0']->message }}} <br>
    </div>
    <div class='col-sm-8'> 
    <h3>Post a Comment:</h3>
        <div class='box'>
        
        <form class='commentform' method="get" action="comment">
        	<b>Name: </b><br>
        	<input type='text' name='name'><br>
        	<b>Message: </b><br>
            <textarea rows='4' cols='22' name='commentmessage'></textarea> <br>
            <?php echo "<button type='submit' name='comment' value='{$post['0']->id }'>Post Comment</button>";?>
        </form>
        </div>
        <h3>Comments:</h3>
        <div class='box'>
        @if (count($results) == 0)
            <p><b>No comments to display.</b></p>
        @else
                    @for ($i=0; $i < count($results); $i++)
                        <div class='commentbox'>
                        <b>Name:</b> {{{ $results[$i]->name }}}<br>
                        <b>Comment:</b> {{{ $results[$i]->comment }}}<br>
                        <button>Delete Comment</button> <br>
                        </div>
                    @endfor
        @endif
        </div>
    </div>
@stop