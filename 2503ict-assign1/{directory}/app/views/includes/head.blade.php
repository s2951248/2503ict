<meta charset="utf-8">
<meta name="description" content="Social Network for Web Programming">
<meta name="keywords" content="Social Network">
<meta name="author" content="Clinton Hill">

<title>Social Network</title>

<!-- CSS stylesheet -->
<link rel="stylesheet" type="text/css" href="css/stylesheet.css">

<!-- Bootstrap core CSS -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css" rel="stylesheet">