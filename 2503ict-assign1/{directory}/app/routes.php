<?php

// Assigns the first page that opens when index.php is called
Route::get('/', function()
{
	$results = postDisplay();
	return View::make('pages.home')->withResults($results);
});

// Adds document.php to the navagation bar
Route::get('document', function() 
{
	return View::make('pages.document');
});

// Function for posting a message on the home page
Route::post('postMessage', function() 
{
	$name = Input::get('name');
	$title = Input::get('title');
	$message = Input::get('message');
	
	$results = dbinsert($name, $title, $message);

	$results = postDisplay();
	
	return View::make('pages.home')->withResults($results);
});

//SQL for posting a message, saves to database under post table
function dbinsert($name, $title, $message){
   	$sql = "INSERT INTO post (name, title, message) VALUES (?,?,?)";
	DB::insert($sql, array($name, $title, $message));
};

//Displays the posts in descending order on the home page
function postDisplay() {
    $results = DB::select('select * from post order by id desc');
    return $results;
};

// Function for editing a post when the save button is pressed
Route::get('editpost', function() 
{
	$postid = Input::get('editpost');
	
	$post = getpost($postid);
	
	return View::make('pages.edit')->withPost($post);
});

//function connected to post editing, selects the correct post to edit
function getpost($postid){
	$sql = "select * from post where id = ?";
	$post = DB::select($sql, array($postid));
	
	return $post;
};

// Redirects to the comments page
Route::get('commentredirect', function(){
	$postid = Input::get('commentredirect');
	
	$post = getpost($postid);
	
	$results = postComment($postid);
	
	return View::make('pages.comment')->withPost($post)->withResults($results);
});

//Function for creating a comment
Route::get('comment', function() 
{
	$name = Input::get('name');
	$commentmessage = Input::get('commentmessage');
	$postid = Input::get('comment');
	
	$post = getpost($postid);
	
	$results = dbcommentinsert($name, $commentmessage, $postid);

	$results = postComment($postid);
	
	return View::make('pages.comment')->withPost($post)->withResults($results);
});

// Inserts the comment into the database with relation to the post
function dbcommentinsert($name, $commentmessage, $postid){
   	$sql = "INSERT INTO comment (name, comment, postid) VALUES (?,?,?)";
    DB::insert($sql, array($name, $commentmessage, $postid));
};
  
// Gets the comments and displays them for the correct post  
function postComment($postid) {
	$sql = "select * from comment where postid = ? order by id desc";
    $results = DB::select($sql, array($postid));
    return $results;
};

// Function for updating the messages on the edit page
Route::get('updateMessage', function() 
{
	$name = Input::get('name');
	$title = Input::get('title');
	$message = Input::get('message');
	$postid = Input::get('edit');
	
	updateMessage($name, $title, $message);
	
	$post = getpost($postid);
	
	$results = postComment($postid);
	
	return View::make('pages.comment')->withPost($post)->withResults($results);
});

// Part of the update messages function
function updateMessage($name, $title, $message){
    if (!empty($name) && (!empty($title)) && (!empty($message))) {
        $sql = "update post set name = ?, title = ?, message = ?";
        DB::update ($sql, array($name, $title, $message));
   	}
};

// Function for deleting comment but didn't work
Route::get('deletecomment', function(){
	$deleteid = Input::get('deletecomment');
	
	$results = getcomment($deleteid);
	
	$post = getpost($deleteid);
	
	return View::make('pages.comment')->withPost($post)->withResults($results);
});

// Gets the comment where postid matches the id of the message
function getcomment($deleteid){
	$sql = "select * from comment where postid = ?";
	$post = DB::select($sql, array($deleteid));
	
	return $post;
};