drop table if exists post;

create table post (
  id integer primary key autoincrement,
  name varchar(40)not null,
  title varchar(40),
  message varchar(255)
);

drop table if exists comment;

create table comment (
  id integer primary key autoincrement,
  name varchar(40) not null,
  comment varchar(255),
  postid varchar(40) not null
);